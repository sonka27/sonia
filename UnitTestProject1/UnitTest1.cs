﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace UnitTestProject1
{
    //[] w tym nawiasie śa atrubuty, ifmroamujemy, ze klasa jest z testami, a metoda z testem
    [TestFixture]
    public class OrangeTest
    {
        IWebDriver driver; // dzieki temu mozemy potem uzyc inny driver niz chrome driver


        //atrybut odnosci sie do pozniszej metody; informatują one o tym: co jest klasa, co jest testem, co jest juz po tescie itd..
        [SetUp]
        public void Setup ()
        {
            driver = new ChromeDriver();
        }


        [Test, Order(2)]
        public void Logowanie()
        {
            driver.Url = "https://opensource-demo.orangehrmlive.com/";
            driver.FindElement(By.XPath("//input[@id='txtUsername']")).SendKeys("Admin");
            driver.FindElement(By.XPath("//input[@id='txtPassword']")).SendKeys("admin123");
            driver.FindElement(By.XPath("//input[@id='btnLogin']")).Click();
            IWebElement welcomeAdmin = driver.FindElement(By.XPath("//a[@id='welcome']"));

            //assert, pokazuje tutaj, że chce zaliczyc test, gdy wynik jest innotnull
            Actions moveMouse = new Actions(driver);
            moveMouse.MoveToElement(welcomeAdmin);
            Assert.IsNotNull(welcomeAdmin); // ten sama klasa jest w visual studio i nunit, wiec albo NuNit.Framework.Assert .. albo wywalamy tą VS

        }

        [Test, Order(1)]
        public void DashboardClick()
        {
            driver.Url = "https://opensource-demo.orangehrmlive.com/";
            driver.FindElement(By.XPath("//input[@id='txtUsername']")).SendKeys("Admin");
            driver.FindElement(By.XPath("//input[@id='txtPassword']")).SendKeys("admin123");
            driver.FindElement(By.XPath("//input[@id='btnLogin']")).Click();
            IWebElement welcomeAdmin = driver.FindElement(By.XPath("//a[@id='welcome']"));

            //assert, pokazuje tutaj, że chce zaliczyc test, gdy wynik jest innotnull
           
            Assert.IsNotNull(welcomeAdmin); // ten sama klasa jest w visual studio i nunit, wiec albo NuNit.Framework.Assert .. albo wywalamy tą VS

            //to jest testowy komentarz, zeby sprawdzic zmiany - EDYTUJEMY TERAZ TO
        }

        //teardown sluzy do tego co ma sie zadziac po

        [TearDown]
        public void TearDown()

        {
            driver.Close();
        }
    }
}
